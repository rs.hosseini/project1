namespace LibrarySoft
{
    partial class BorrowBook_Frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.txt_member = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_book = new System.Windows.Forms.TextBox();
            this.btn_search_member = new System.Windows.Forms.Button();
            this.btn_search_book = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txt_search = new System.Windows.Forms.TextBox();
            this.Gridview_Borrow = new System.Windows.Forms.DataGridView();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_edit = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_new = new System.Windows.Forms.Button();
            this.lbl_count = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_sdate = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_modat = new System.Windows.Forms.TextBox();
            this.txt_edate = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Gridview_Borrow)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label4.ForeColor = System.Drawing.Color.Crimson;
            this.label4.Location = new System.Drawing.Point(823, 17);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(111, 20);
            this.label4.TabIndex = 97;
            this.label4.Text = "عضو را انتخاب کنید :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_member
            // 
            this.txt_member.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.txt_member.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_member.Location = new System.Drawing.Point(612, 14);
            this.txt_member.Name = "txt_member";
            this.txt_member.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_member.Size = new System.Drawing.Size(210, 27);
            this.txt_member.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.ForeColor = System.Drawing.Color.Crimson;
            this.label1.Location = new System.Drawing.Point(256, 16);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(112, 20);
            this.label1.TabIndex = 99;
            this.label1.Text = "کتاب را انتخاب کنید :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_book
            // 
            this.txt_book.BackColor = System.Drawing.Color.PowderBlue;
            this.txt_book.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_book.Location = new System.Drawing.Point(45, 13);
            this.txt_book.Name = "txt_book";
            this.txt_book.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_book.Size = new System.Drawing.Size(210, 27);
            this.txt_book.TabIndex = 1;
            // 
            // btn_search_member
            // 
            this.btn_search_member.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_search_member.ForeColor = System.Drawing.Color.White;
            this.btn_search_member.Image = global::LibrarySoft.Properties.Resources.SmallAdd;
            this.btn_search_member.Location = new System.Drawing.Point(574, 11);
            this.btn_search_member.Name = "btn_search_member";
            this.btn_search_member.Size = new System.Drawing.Size(35, 33);
            this.btn_search_member.TabIndex = 100;
            this.btn_search_member.UseVisualStyleBackColor = true;
            this.btn_search_member.Click += new System.EventHandler(this.btn_search_member_Click);
            // 
            // btn_search_book
            // 
            this.btn_search_book.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_search_book.ForeColor = System.Drawing.Color.White;
            this.btn_search_book.Image = global::LibrarySoft.Properties.Resources.SmallAdd;
            this.btn_search_book.Location = new System.Drawing.Point(7, 8);
            this.btn_search_book.Name = "btn_search_book";
            this.btn_search_book.Size = new System.Drawing.Size(35, 33);
            this.btn_search_book.TabIndex = 101;
            this.btn_search_book.UseVisualStyleBackColor = true;
            this.btn_search_book.Click += new System.EventHandler(this.btn_search_book_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::LibrarySoft.Properties.Resources.search;
            this.pictureBox1.Location = new System.Drawing.Point(909, 216);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 23);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 104;
            this.pictureBox1.TabStop = false;
            // 
            // txt_search
            // 
            this.txt_search.BackColor = System.Drawing.Color.Gainsboro;
            this.txt_search.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_search.Location = new System.Drawing.Point(6, 216);
            this.txt_search.Name = "txt_search";
            this.txt_search.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_search.Size = new System.Drawing.Size(894, 27);
            this.txt_search.TabIndex = 102;
            this.txt_search.TextChanged += new System.EventHandler(this.txt_search_TextChanged);
            // 
            // Gridview_Borrow
            // 
            this.Gridview_Borrow.AllowUserToAddRows = false;
            this.Gridview_Borrow.AllowUserToDeleteRows = false;
            this.Gridview_Borrow.AllowUserToResizeColumns = false;
            this.Gridview_Borrow.AllowUserToResizeRows = false;
            this.Gridview_Borrow.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Gridview_Borrow.ColumnHeadersHeight = 37;
            this.Gridview_Borrow.Location = new System.Drawing.Point(6, 249);
            this.Gridview_Borrow.Name = "Gridview_Borrow";
            this.Gridview_Borrow.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Gridview_Borrow.Size = new System.Drawing.Size(934, 292);
            this.Gridview_Borrow.TabIndex = 103;
            this.Gridview_Borrow.Click += new System.EventHandler(this.Gridview_Book_Click);
            this.Gridview_Borrow.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Gridview_Book_KeyDown);
            this.Gridview_Borrow.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Gridview_Borrow_KeyUp);
            // 
            // btn_cancel
            // 
            this.btn_cancel.Image = global::LibrarySoft.Properties.Resources.cancel;
            this.btn_cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cancel.Location = new System.Drawing.Point(381, 548);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(112, 43);
            this.btn_cancel.TabIndex = 144;
            this.btn_cancel.Text = "انصراف";
            this.btn_cancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.Image = global::LibrarySoft.Properties.Resources.delete;
            this.btn_delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_delete.Location = new System.Drawing.Point(493, 548);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(112, 43);
            this.btn_delete.TabIndex = 143;
            this.btn_delete.Text = "حذف امانات";
            this.btn_delete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_edit
            // 
            this.btn_edit.Image = global::LibrarySoft.Properties.Resources.edit;
            this.btn_edit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_edit.Location = new System.Drawing.Point(605, 548);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(112, 43);
            this.btn_edit.TabIndex = 142;
            this.btn_edit.Text = "ویرایش امانات";
            this.btn_edit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_edit.UseVisualStyleBackColor = true;
            this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
            // 
            // btn_save
            // 
            this.btn_save.Image = global::LibrarySoft.Properties.Resources.Save_Icon;
            this.btn_save.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_save.Location = new System.Drawing.Point(717, 548);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(112, 43);
            this.btn_save.TabIndex = 5;
            this.btn_save.Text = "ثبت امانات";
            this.btn_save.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_new
            // 
            this.btn_new.Image = global::LibrarySoft.Properties.Resources.select;
            this.btn_new.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_new.Location = new System.Drawing.Point(829, 548);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(112, 43);
            this.btn_new.TabIndex = 141;
            this.btn_new.Text = "امانات جدید";
            this.btn_new.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_new.UseVisualStyleBackColor = true;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // lbl_count
            // 
            this.lbl_count.AutoSize = true;
            this.lbl_count.Font = new System.Drawing.Font("B Yekan", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lbl_count.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lbl_count.Location = new System.Drawing.Point(9, 557);
            this.lbl_count.Name = "lbl_count";
            this.lbl_count.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_count.Size = new System.Drawing.Size(23, 27);
            this.lbl_count.TabIndex = 146;
            this.lbl_count.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label9.ForeColor = System.Drawing.Color.Crimson;
            this.label9.Location = new System.Drawing.Point(40, 562);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(167, 20);
            this.label9.TabIndex = 145;
            this.label9.Text = "تعداد کتاب های امانت داده شده :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(758, 140);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(27, 20);
            this.label12.TabIndex = 151;
            this.label12.Text = "روز";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(866, 141);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(68, 20);
            this.label10.TabIndex = 150;
            this.label10.Text = "مدت امانت :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_sdate
            // 
            this.txt_sdate.BackColor = System.Drawing.Color.White;
            this.txt_sdate.Font = new System.Drawing.Font("B Yekan", 9.75F);
            this.txt_sdate.Location = new System.Drawing.Point(716, 74);
            this.txt_sdate.Mask = "####/##/##";
            this.txt_sdate.Name = "txt_sdate";
            this.txt_sdate.Size = new System.Drawing.Size(115, 27);
            this.txt_sdate.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(833, 77);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(101, 20);
            this.label8.TabIndex = 149;
            this.label8.Text = "تاریخ شروع امانت :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_modat
            // 
            this.txt_modat.BackColor = System.Drawing.Color.Gold;
            this.txt_modat.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_modat.Location = new System.Drawing.Point(785, 137);
            this.txt_modat.Name = "txt_modat";
            this.txt_modat.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_modat.Size = new System.Drawing.Size(46, 27);
            this.txt_modat.TabIndex = 4;
            this.txt_modat.Text = "0";
            this.txt_modat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_edate
            // 
            this.txt_edate.BackColor = System.Drawing.Color.White;
            this.txt_edate.Font = new System.Drawing.Font("B Yekan", 9.75F);
            this.txt_edate.Location = new System.Drawing.Point(716, 106);
            this.txt_edate.Mask = "####/##/##";
            this.txt_edate.Name = "txt_edate";
            this.txt_edate.Size = new System.Drawing.Size(115, 27);
            this.txt_edate.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(839, 109);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(95, 20);
            this.label2.TabIndex = 153;
            this.label2.Text = "تاریخ پایان امانت :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BorrowBook_Frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(945, 596);
            this.Controls.Add(this.txt_edate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txt_sdate);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txt_modat);
            this.Controls.Add(this.lbl_count);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_edit);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.btn_new);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txt_search);
            this.Controls.Add(this.Gridview_Borrow);
            this.Controls.Add(this.btn_search_book);
            this.Controls.Add(this.btn_search_member);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_book);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_member);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "BorrowBook_Frm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فرم امانت گرفتن کتاب";
            this.Load += new System.EventHandler(this.BorrowBook_Frm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Gridview_Borrow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_member;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_book;
        private System.Windows.Forms.Button btn_search_member;
        private System.Windows.Forms.Button btn_search_book;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txt_search;
        private System.Windows.Forms.DataGridView Gridview_Borrow;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_edit;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.Label lbl_count;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox txt_sdate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_modat;
        private System.Windows.Forms.MaskedTextBox txt_edate;
        private System.Windows.Forms.Label label2;
    }
}