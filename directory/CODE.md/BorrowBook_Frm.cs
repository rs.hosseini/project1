using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LibrarySoft
{
    public partial class BorrowBook_Frm : Form
    {
        public string id, sh_member, sh_book;
        Class_DBS dbs = new Class_DBS();
        DataTable dt = new DataTable();

        public BorrowBook_Frm()
        {
            InitializeComponent();
        }

        public void CountRows()
        {
            lbl_count.Text = Gridview_Borrow.Rows.Count.ToString();
        }
       
        public void HeaderEdit()
        {
            Gridview_Borrow.Columns[1].HeaderText = "شناسه کتاب";
            Gridview_Borrow.Columns[2].HeaderText = "شناسه عضویت";
            Gridview_Borrow.Columns[3].HeaderText = "نام عضو";
            Gridview_Borrow.Columns[4].HeaderText = "نام خانوادگی";
            Gridview_Borrow.Columns[5].HeaderText = "نام کتاب";
            Gridview_Borrow.Columns[6].HeaderText = "موضوع کتاب";
            Gridview_Borrow.Columns[8].HeaderText = "نویسنده";
            Gridview_Borrow.Columns[9].HeaderText = "تاریخ شروع";
            Gridview_Borrow.Columns[10].HeaderText = "تاریخ پایان";
            Gridview_Borrow.Columns[11].HeaderText = "مدت امانت";
            Gridview_Borrow.Columns[14].HeaderText = "وضعیت امانت";

            Gridview_Borrow.Columns[1].Width = 70;
            Gridview_Borrow.Columns[2].Width = 70;
            Gridview_Borrow.Columns[3].Width = 90;
            Gridview_Borrow.Columns[4].Width = 100;
            Gridview_Borrow.Columns[5].Width = 120;
            Gridview_Borrow.Columns[6].Width = 110;
            Gridview_Borrow.Columns[8].Width = 100;
            Gridview_Borrow.Columns[9].Width = 65;
            Gridview_Borrow.Columns[10].Width = 65;
            Gridview_Borrow.Columns[11].Width = 40;
            Gridview_Borrow.Columns[14].Width = 60;

            Gridview_Borrow.Columns[0].Visible = false;
            Gridview_Borrow.Columns[7].Visible = false;
            Gridview_Borrow.Columns[12].Visible = false;
            Gridview_Borrow.Columns[13].Visible = false;
            Gridview_Borrow.Columns[15].Visible = false;
        }
       

        public void EBtn()
        {
            btn_new.Enabled = true;
            btn_save.Enabled = false;
            btn_edit.Enabled = true;
            btn_delete.Enabled = true;
        }

        public void DBtn()
        {
            btn_new.Enabled = false;
            btn_save.Enabled = true;
            btn_edit.Enabled = false;
            btn_delete.Enabled = false;
        }

        public void FullGrid_Borrow()
        {
            dbs.connect();
            Gridview_Borrow.DataSource = dbs.show("Select * From View_Borrow");
            dbs.disconnect();
            HeaderEdit();
        }

        public void Clear()
        {
            txt_modat.Text = "0";
            txt_sdate.Text = null;
            txt_edate.Text = null;
        }

        public void IndexGridView()
        {
            id = Gridview_Borrow[0, Gridview_Borrow.CurrentRow.Index].Value.ToString();
            sh_book = Gridview_Borrow[1, Gridview_Borrow.CurrentRow.Index].Value.ToString();
            sh_member = Gridview_Borrow[2, Gridview_Borrow.CurrentRow.Index].Value.ToString();
            txt_member.Text = Gridview_Borrow[3, Gridview_Borrow.CurrentRow.Index].Value.ToString() + " " + Gridview_Borrow[4, Gridview_Borrow.CurrentRow.Index].Value.ToString();
            txt_book.Text = Gridview_Borrow[5, Gridview_Borrow.CurrentRow.Index].Value.ToString();
            txt_sdate.Text = Gridview_Borrow[9, Gridview_Borrow.CurrentRow.Index].Value.ToString();
            txt_edate.Text = Gridview_Borrow[10, Gridview_Borrow.CurrentRow.Index].Value.ToString();
            txt_modat.Text = Gridview_Borrow[11, Gridview_Borrow.CurrentRow.Index].Value.ToString();

        }

        private void btn_search_member_Click(object sender, EventArgs e)
        {
            SearchMember_Frm smf = new SearchMember_Frm();
            smf.ShowDialog();
            txt_member.Text = smf.name;
            sh_member = smf.shenas;
        }

        private void btn_search_book_Click(object sender, EventArgs e)
        {
            SearchBook_Frm sbf = new SearchBook_Frm();
            sbf.ShowDialog();
            txt_book.Text = sbf.name_book;
            sh_book = sbf.shenas;
        }

        private void BorrowBook_Frm_Load(object sender, EventArgs e)
        {
            FullGrid_Borrow();
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            txt_sdate.Focus();
            Clear();
            dbs.disconnect();
            FullGrid_Borrow();
            DBtn();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            dbs.connect();
            dbs.execute("Insert Into BorrowInfo Values('" + sh_book + "','" + sh_member + "','" + txt_sdate.Text + "','" + txt_edate.Text + "','" + txt_modat.Text + "','" + 0 + "','" + 0 + "','" + false + "')");
            dbs.disconnect();
            FullGrid_Borrow();
            EBtn();
            Clear();
            CountRows();
            MessageBox.Show("کاربر محترم عملیات ثبت امانت با موفقیت انجام شد", "پیغام", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btn_edit_Click(object sender, EventArgs e)
        {
            dbs.connect();
            dbs.execute("Update BorrowInfo Set shenas_book='" + sh_book + "',shenas_member='" + sh_member + "',sdate='" + txt_sdate.Text + "',edate='" + txt_edate.Text + "',day='" + txt_modat.Text + "' Where ID='" + id + "'");
            dbs.disconnect();
            FullGrid_Borrow();
            EBtn();
            Clear();
            MessageBox.Show("کاربر محترم عملیات ثبت امانت با موفقیت انجام شد", "پیغام", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("کاربر محترم آیا مطمئن به حذف امانت مورد نظر می باشید؟", "هشدار", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                dbs.connect();
                dbs.execute("Delete From BorrowInfo Where ID='" + id + "'");
                dbs.disconnect();
                FullGrid_Borrow();
                EBtn();
                CountRows();
                Clear();
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Clear();
            dbs.disconnect();
            FullGrid_Borrow();
            EBtn();
        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            dbs.connect();
            Gridview_Borrow.DataSource = dbs.show("Select * From View_Borrow Where shenas_book Like '%" + txt_search.Text + "%' or shenas_member Like '%" + txt_search.Text + "%' or lname Like N'%" + txt_search.Text + "%' or name_book Like N'%" + txt_search.Text + "%' or sdate Like '%" + txt_search.Text + "%'");
            dbs.disconnect();
            HeaderEdit();
            CountRows();
        }

        private void Gridview_Book_Click(object sender, EventArgs e)
        {
            IndexGridView();
        }

        private void Gridview_Book_KeyDown(object sender, KeyEventArgs e)
        {
            IndexGridView();
        }

        private void Gridview_Borrow_KeyUp(object sender, KeyEventArgs e)
        {
            IndexGridView();
        }




    }
}
