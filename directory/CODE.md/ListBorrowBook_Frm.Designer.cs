namespace LibrarySoft
{
    partial class ListBorrowBook_Frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Gridview_Borrow = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.Gridview_Borrow)).BeginInit();
            this.SuspendLayout();
            // 
            // Gridview_Borrow
            // 
            this.Gridview_Borrow.AllowUserToAddRows = false;
            this.Gridview_Borrow.AllowUserToDeleteRows = false;
            this.Gridview_Borrow.AllowUserToResizeColumns = false;
            this.Gridview_Borrow.AllowUserToResizeRows = false;
            this.Gridview_Borrow.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Gridview_Borrow.ColumnHeadersHeight = 37;
            this.Gridview_Borrow.Location = new System.Drawing.Point(6, 8);
            this.Gridview_Borrow.Name = "Gridview_Borrow";
            this.Gridview_Borrow.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Gridview_Borrow.Size = new System.Drawing.Size(662, 484);
            this.Gridview_Borrow.TabIndex = 103;
            // 
            // ListBorrowBook_Frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(673, 497);
            this.Controls.Add(this.Gridview_Borrow);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "ListBorrowBook_Frm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "لیست کتاب های به امانت گرفته شده";
            this.Load += new System.EventHandler(this.ListBorrowBook_Frm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Gridview_Borrow)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView Gridview_Borrow;
    }
}