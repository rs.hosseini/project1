using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LibrarySoft
{
    public partial class ListBorrowBook_Frm : Form
    {
        public string id, sh_member;
        Class_DBS dbs = new Class_DBS();
        DataTable dt = new DataTable();

        public ListBorrowBook_Frm()
        {
            InitializeComponent();
        }

        public void HeaderEdit()
        {
            Gridview_Borrow.Columns[1].HeaderText = "شناسه کتاب";
            Gridview_Borrow.Columns[5].HeaderText = "نام کتاب";
            Gridview_Borrow.Columns[6].HeaderText = "موضوع کتاب";
            Gridview_Borrow.Columns[8].HeaderText = "نویسنده";
            Gridview_Borrow.Columns[9].HeaderText = "تاریخ شروع";
            Gridview_Borrow.Columns[10].HeaderText = "تاریخ پایان";
            Gridview_Borrow.Columns[11].HeaderText = "مدت امانت";
            Gridview_Borrow.Columns[14].HeaderText = "وضعیت امانت";

            Gridview_Borrow.Columns[1].Width = 66;
            Gridview_Borrow.Columns[5].Width = 120;
            Gridview_Borrow.Columns[6].Width = 103;
            Gridview_Borrow.Columns[8].Width = 100;
            Gridview_Borrow.Columns[9].Width = 65;
            Gridview_Borrow.Columns[10].Width = 65;
            Gridview_Borrow.Columns[11].Width = 40;
            Gridview_Borrow.Columns[14].Width = 60;

            Gridview_Borrow.Columns[0].Visible = false;
            Gridview_Borrow.Columns[2].Visible = false;
            Gridview_Borrow.Columns[3].Visible = false;
            Gridview_Borrow.Columns[4].Visible = false;
            Gridview_Borrow.Columns[7].Visible = false;
            Gridview_Borrow.Columns[12].Visible = false;
            Gridview_Borrow.Columns[13].Visible = false;
            Gridview_Borrow.Columns[15].Visible = false;
        }
      


        public void FullGrid_Borrow()
        {
            dbs.connect();
            Gridview_Borrow.DataSource = dbs.show("Select * From View_Borrow Where shenas_member='" + sh_member + "'");
            dbs.disconnect();
            HeaderEdit();
        }

        private void ListBorrowBook_Frm_Load(object sender, EventArgs e)
        {
            FullGrid_Borrow();
        }




    }
}
