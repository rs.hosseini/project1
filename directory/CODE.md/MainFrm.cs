using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LibrarySoft
{
    public partial class MainFrm : Form
    {
        public MainFrm()
        {
            InitializeComponent();
        }

        private void btn_member_Click(object sender, EventArgs e)
        {
            Member_Frm mf = new Member_Frm();
            mf.MdiParent = this;
            mf.Show();
        }

        private void btn_book_Click(object sender, EventArgs e)
        {
            Book_Frm bf = new Book_Frm();
            bf.MdiParent = this;
            bf.Show();
        }

        private void btn_persoenl_Click(object sender, EventArgs e)
        {
            Personel_Frm pf = new Personel_Frm();
            pf.MdiParent = this;
            pf.Show();
        }

        private void btn_amanat_Click(object sender, EventArgs e)
        {
            BorrowBook_Frm bbf = new BorrowBook_Frm();
            bbf.MdiParent = this;
            bbf.Show();
        }

        private void btn_back_amanat_Click(object sender, EventArgs e)
        {
            BackBorrowBook_Frm bbbf = new BackBorrowBook_Frm();
            bbbf.MdiParent = this;
            bbbf.Show();
        }

        private void btn_setting_Click(object sender, EventArgs e)
        {

        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MainFrm_Load(object sender, EventArgs e)
        {

        }

    }
}