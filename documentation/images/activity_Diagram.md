فاز2: صفحه اطلاعات اعضا برای ثبت و ویرایش عضو توسط مدیر

![activityhazfozv](/uploads/f37ee5f3c0965cf27446e3ae7a3d6ff2/activityhazfozv.png)
![activitysabtozv](/uploads/6ee6647a1a451660c08e1af2e7ec6b70/activitysabtozv.png)
![activityvirayeshozv](/uploads/d59697679eaa31e0612ba7b604d8ac40/activityvirayeshozv.png)


فاز3: صفحه اطلاعات کتابها برای ثبت و ویرایش کتابها توسط مدیر

![activityhazfketab](/uploads/15c26be2ee56153744d3e72c4c774ffa/activityhazfketab.png)
![activitysabtketab](/uploads/5992dec95d0b0b12bd2139caa4f33b7c/activitysabtketab.png)
![activityvirayeshketab](/uploads/4e87683ea9e91b072353a0c61df99131/activityvirayeshketab.png)

فاز4: گزارش گیری

![activityBakap](/uploads/b48e88b72ff73c0c98e3ccef017b1b9c/activityBakap.png)

فاز5: بخش امانات

![acrityamanat](/uploads/79a7efcb23916138aaa982f4d7abf9dd/acrityamanat.jpg)
![acritybargasht](/uploads/eea4c14b03cc178213ed7f7febfaa3f9/acritybargasht.jpg)
