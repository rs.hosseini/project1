فاز2: صفحه اطلاعات اعضا برای ثبت و ویرایش عضو توسط مدیر

![newcostomer](/uploads/df36a10f12e09061a00d49cb331b9840/newcostomer.png)


فاز3: صفحه اطلاعات کتابها برای ثبت و ویرایش کتابها توسط مدیر

![desSabteketab](/uploads/7fc52b89494b68a497715a69b15a6958/desSabteketab.png)
![deshazfketab](/uploads/63033be6131e8c2882a55a76b91b63e6/deshazfketab.png)

فاز4: گزارش گیری

![baCAP322](/uploads/c85cabdf67b031e553271206e0abd6d3/baCAP322.jpg)

فاز5: بخش امانات

![bargashtamanat](/uploads/5add0675abf43cebd4783f439a3e802f/bargashtamanat.jpg)
![amanat](/uploads/f49f86ddbd833692e42b297b3c1163d8/amanat.jpg)