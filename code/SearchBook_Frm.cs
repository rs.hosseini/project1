﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LibrarySoft
{
    public partial class SearchBook_Frm : Form
    {
        public string shenas, name_book;
        public SearchBook_Frm()
        {
            InitializeComponent();
        }

        Class_DBS dbs = new Class_DBS();

        public void HeaderEdit()
        {
            Gridview_Book.Columns[1].HeaderText = "شناسه کتاب";
            Gridview_Book.Columns[2].HeaderText = "نام کتاب";
            Gridview_Book.Columns[4].HeaderText = "عنوان کتاب";
            Gridview_Book.Columns[5].HeaderText = "نویسنده";

            Gridview_Book.Columns[1].Width = 53;
            Gridview_Book.Columns[2].Width = 140;
            Gridview_Book.Columns[4].Width = 115;
            Gridview_Book.Columns[5].Width = 120;

            Gridview_Book.Columns[0].Visible = false;
            Gridview_Book.Columns[3].Visible = false;
            Gridview_Book.Columns[6].Visible = false;
            Gridview_Book.Columns[7].Visible = false;
            Gridview_Book.Columns[8].Visible = false;
            Gridview_Book.Columns[9].Visible = false;
        }
        //...تابع ویرایش بخش هدر گریدویو

        public void FullGrid_Book()
        {
            dbs.connect();
            Gridview_Book.DataSource = dbs.show("Select * From BookInfo");
            dbs.disconnect();
            HeaderEdit();
        }

        public void RowIndex()
        {
            shenas = Gridview_Book[1, Gridview_Book.CurrentRow.Index].Value.ToString();
            name_book = Gridview_Book[2, Gridview_Book.CurrentRow.Index].Value.ToString();
        }


        private void SearchMember_Frm_Load(object sender, EventArgs e)
        {
            FullGrid_Book();
        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            dbs.connect();
            Gridview_Book.DataSource = dbs.show("Select * From BookInfo Where shenas_book Like '%" + txt_search.Text + "%' or name_book Like N'%" + txt_search.Text + "%' or subject_book Like N'%" + txt_search.Text + "%' or type_book Like N'%" + txt_search.Text + "%' or auther Like N'%" + txt_search.Text + "%'");
            dbs.disconnect();
            HeaderEdit();
        }

        private void Gridview_Book_DoubleClick(object sender, EventArgs e)
        {
            RowIndex();
            this.Close();
        }
    }
}
