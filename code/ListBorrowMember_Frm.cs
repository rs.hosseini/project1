﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LibrarySoft
{
    public partial class ListBorrowMember_Frm : Form
    {
        public string id, sh_book;
        Class_DBS dbs = new Class_DBS();
        DataTable dt = new DataTable();

        public ListBorrowMember_Frm()
        {
            InitializeComponent();
        }

        public void HeaderEdit()
        {
            Gridview_Borrow.Columns[2].HeaderText = "شناسه عضویت";
            Gridview_Borrow.Columns[3].HeaderText = "نام عضو";
            Gridview_Borrow.Columns[4].HeaderText = "نام خانوادگی";
            Gridview_Borrow.Columns[9].HeaderText = "تاریخ شروع";
            Gridview_Borrow.Columns[10].HeaderText = "تاریخ پایان";
            Gridview_Borrow.Columns[11].HeaderText = "مدت امانت";
            Gridview_Borrow.Columns[14].HeaderText = "وضعیت امانت";

            Gridview_Borrow.Columns[2].Width = 90;
            Gridview_Borrow.Columns[3].Width = 105;
            Gridview_Borrow.Columns[4].Width = 134;
            Gridview_Borrow.Columns[9].Width = 80;
            Gridview_Borrow.Columns[10].Width = 80;
            Gridview_Borrow.Columns[11].Width = 60;
            Gridview_Borrow.Columns[14].Width = 70;

            Gridview_Borrow.Columns[0].Visible = false;
            Gridview_Borrow.Columns[1].Visible = false;
            Gridview_Borrow.Columns[5].Visible = false;
            Gridview_Borrow.Columns[6].Visible = false;
            Gridview_Borrow.Columns[8].Visible = false;
            Gridview_Borrow.Columns[7].Visible = false;
            Gridview_Borrow.Columns[12].Visible = false;
            Gridview_Borrow.Columns[13].Visible = false;
            Gridview_Borrow.Columns[15].Visible = false;
        }
        //...تابع ویرایش بخش هدر گریدویو


        public void FullGrid_Borrow()
        {
            dbs.connect();
            Gridview_Borrow.DataSource = dbs.show("Select * From View_Borrow Where shenas_book='" + sh_book + "'");
            dbs.disconnect();
            HeaderEdit();
        }

        private void ListBorrowBook_Frm_Load(object sender, EventArgs e)
        {
            FullGrid_Borrow();
        }




    }
}
