﻿namespace LibrarySoft
{
    partial class Member_Frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Gridview_Member = new System.Windows.Forms.DataGridView();
            this.txt_search = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_shenas = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lbl_count = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_fname = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_lname = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_pname = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_code_meli = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txt_modat = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_email = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txt_tel = new System.Windows.Forms.TextBox();
            this.txt_sdate = new System.Windows.Forms.MaskedTextBox();
            this.com_madrak = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_address = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txt_price = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_edate = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btn_list_book = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_edit = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_new = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Gridview_Member)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Gridview_Member
            // 
            this.Gridview_Member.AllowUserToAddRows = false;
            this.Gridview_Member.AllowUserToDeleteRows = false;
            this.Gridview_Member.AllowUserToResizeColumns = false;
            this.Gridview_Member.AllowUserToResizeRows = false;
            this.Gridview_Member.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Gridview_Member.ColumnHeadersHeight = 37;
            this.Gridview_Member.Location = new System.Drawing.Point(4, 332);
            this.Gridview_Member.Name = "Gridview_Member";
            this.Gridview_Member.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Gridview_Member.Size = new System.Drawing.Size(948, 222);
            this.Gridview_Member.TabIndex = 14;
            this.Gridview_Member.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.Gridview_Member_CellFormatting);
            this.Gridview_Member.Click += new System.EventHandler(this.Gridview_Member_Click);
            this.Gridview_Member.DoubleClick += new System.EventHandler(this.Gridview_Member_DoubleClick);
            this.Gridview_Member.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Gridview_Member_KeyDown);
            this.Gridview_Member.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Gridview_Member_KeyUp);
            // 
            // txt_search
            // 
            this.txt_search.BackColor = System.Drawing.Color.Gainsboro;
            this.txt_search.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_search.Location = new System.Drawing.Point(4, 302);
            this.txt_search.Name = "txt_search";
            this.txt_search.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_search.Size = new System.Drawing.Size(912, 27);
            this.txt_search.TabIndex = 13;
            this.txt_search.TextChanged += new System.EventHandler(this.txt_search_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(859, 13);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(85, 20);
            this.label4.TabIndex = 95;
            this.label4.Text = "شماره عضویت :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_shenas
            // 
            this.txt_shenas.BackColor = System.Drawing.Color.LightCyan;
            this.txt_shenas.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_shenas.Location = new System.Drawing.Point(663, 10);
            this.txt_shenas.Name = "txt_shenas";
            this.txt_shenas.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_shenas.Size = new System.Drawing.Size(192, 27);
            this.txt_shenas.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label9.ForeColor = System.Drawing.Color.Crimson;
            this.label9.Location = new System.Drawing.Point(45, 570);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(117, 20);
            this.label9.TabIndex = 102;
            this.label9.Text = "تعداد اعضاء ثبت شده :";
            // 
            // lbl_count
            // 
            this.lbl_count.AutoSize = true;
            this.lbl_count.Font = new System.Drawing.Font("B Yekan", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lbl_count.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lbl_count.Location = new System.Drawing.Point(9, 565);
            this.lbl_count.Name = "lbl_count";
            this.lbl_count.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_count.Size = new System.Drawing.Size(23, 27);
            this.lbl_count.TabIndex = 103;
            this.lbl_count.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(914, 44);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(30, 20);
            this.label2.TabIndex = 109;
            this.label2.Text = "نام :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_fname
            // 
            this.txt_fname.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_fname.Location = new System.Drawing.Point(663, 41);
            this.txt_fname.Name = "txt_fname";
            this.txt_fname.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_fname.Size = new System.Drawing.Size(192, 27);
            this.txt_fname.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(868, 75);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(76, 20);
            this.label3.TabIndex = 111;
            this.label3.Text = "نام خانوادگی :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_lname
            // 
            this.txt_lname.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_lname.Location = new System.Drawing.Point(663, 72);
            this.txt_lname.Name = "txt_lname";
            this.txt_lname.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_lname.Size = new System.Drawing.Size(192, 27);
            this.txt_lname.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(894, 102);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(50, 20);
            this.label5.TabIndex = 113;
            this.label5.Text = "نام پدر :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_pname
            // 
            this.txt_pname.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_pname.Location = new System.Drawing.Point(663, 102);
            this.txt_pname.MaxLength = 10;
            this.txt_pname.Name = "txt_pname";
            this.txt_pname.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_pname.Size = new System.Drawing.Size(192, 27);
            this.txt_pname.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(859, 136);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(85, 20);
            this.label6.TabIndex = 115;
            this.label6.Text = "مدرک تحصیلی :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(896, 168);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(48, 20);
            this.label7.TabIndex = 117;
            this.label7.Text = "کد ملّی :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_code_meli
            // 
            this.txt_code_meli.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_code_meli.Location = new System.Drawing.Point(663, 165);
            this.txt_code_meli.MaxLength = 10;
            this.txt_code_meli.Name = "txt_code_meli";
            this.txt_code_meli.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_code_meli.Size = new System.Drawing.Size(192, 27);
            this.txt_code_meli.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label13.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label13.Location = new System.Drawing.Point(153, 34);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(80, 20);
            this.label13.TabIndex = 127;
            this.label13.Text = "تاریخ عضویت :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_modat
            // 
            this.txt_modat.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.txt_modat.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_modat.Location = new System.Drawing.Point(98, 104);
            this.txt_modat.Name = "txt_modat";
            this.txt_modat.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_modat.Size = new System.Drawing.Size(53, 27);
            this.txt_modat.TabIndex = 12;
            this.txt_modat.Text = "0";
            this.txt_modat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(906, 199);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(38, 20);
            this.label14.TabIndex = 129;
            this.label14.Text = "تلفن :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_tel
            // 
            this.txt_tel.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_tel.Location = new System.Drawing.Point(663, 196);
            this.txt_tel.Name = "txt_tel";
            this.txt_tel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_tel.Size = new System.Drawing.Size(192, 27);
            this.txt_tel.TabIndex = 6;
            // 
            // txt_sdate
            // 
            this.txt_sdate.BackColor = System.Drawing.Color.Khaki;
            this.txt_sdate.Font = new System.Drawing.Font("B Yekan", 9.75F);
            this.txt_sdate.Location = new System.Drawing.Point(52, 31);
            this.txt_sdate.Mask = "####/##/##";
            this.txt_sdate.Name = "txt_sdate";
            this.txt_sdate.Size = new System.Drawing.Size(99, 27);
            this.txt_sdate.TabIndex = 10;
            // 
            // com_madrak
            // 
            this.com_madrak.Font = new System.Drawing.Font("B Yekan", 9.75F);
            this.com_madrak.FormattingEnabled = true;
            this.com_madrak.Items.AddRange(new object[] {
            "زیردیپلم",
            "دیپلم",
            "فوق دیپلم",
            "لیسانس",
            "فوق لیسانس",
            "دکتری",
            "مدارک بالاتر"});
            this.com_madrak.Location = new System.Drawing.Point(663, 133);
            this.com_madrak.Name = "com_madrak";
            this.com_madrak.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.com_madrak.Size = new System.Drawing.Size(193, 28);
            this.com_madrak.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.txt_price);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txt_edate);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txt_sdate);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txt_modat);
            this.groupBox1.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.groupBox1.Location = new System.Drawing.Point(12, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox1.Size = new System.Drawing.Size(242, 178);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "عضویت";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label15.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label15.Location = new System.Drawing.Point(19, 142);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(31, 20);
            this.label15.TabIndex = 134;
            this.label15.Text = "ریال";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label16.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label16.Location = new System.Drawing.Point(157, 142);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(76, 20);
            this.label16.TabIndex = 133;
            this.label16.Text = "مبلغ عضویت :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_price
            // 
            this.txt_price.BackColor = System.Drawing.Color.SkyBlue;
            this.txt_price.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_price.Location = new System.Drawing.Point(52, 138);
            this.txt_price.Name = "txt_price";
            this.txt_price.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_price.Size = new System.Drawing.Size(99, 27);
            this.txt_price.TabIndex = 13;
            this.txt_price.Text = "0";
            this.txt_price.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label12.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label12.Location = new System.Drawing.Point(72, 107);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(22, 20);
            this.label12.TabIndex = 131;
            this.label12.Text = "ماه";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label10.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label10.Location = new System.Drawing.Point(155, 108);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(78, 20);
            this.label10.TabIndex = 130;
            this.label10.Text = "مدت عضویت :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_edate
            // 
            this.txt_edate.BackColor = System.Drawing.Color.Khaki;
            this.txt_edate.Font = new System.Drawing.Font("B Yekan", 9.75F);
            this.txt_edate.Location = new System.Drawing.Point(52, 68);
            this.txt_edate.Mask = "####/##/##";
            this.txt_edate.Name = "txt_edate";
            this.txt_edate.Size = new System.Drawing.Size(99, 27);
            this.txt_edate.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label8.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label8.Location = new System.Drawing.Point(166, 71);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(67, 20);
            this.label8.TabIndex = 129;
            this.label8.Text = "تاریخ اتمام :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btn_list_book
            // 
            this.btn_list_book.Image = global::LibrarySoft.Properties.Resources.report_factor;
            this.btn_list_book.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_list_book.Location = new System.Drawing.Point(243, 557);
            this.btn_list_book.Name = "btn_list_book";
            this.btn_list_book.Size = new System.Drawing.Size(190, 43);
            this.btn_list_book.TabIndex = 140;
            this.btn_list_book.Text = "لیست کتابهای امانت برده شده";
            this.btn_list_book.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_list_book.UseVisualStyleBackColor = true;
            this.btn_list_book.Click += new System.EventHandler(this.btn_list_book_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.Image = global::LibrarySoft.Properties.Resources.cancel;
            this.btn_cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cancel.Location = new System.Drawing.Point(432, 557);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(105, 43);
            this.btn_cancel.TabIndex = 139;
            this.btn_cancel.Text = "انصراف";
            this.btn_cancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.Image = global::LibrarySoft.Properties.Resources.delete;
            this.btn_delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_delete.Location = new System.Drawing.Point(536, 557);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(105, 43);
            this.btn_delete.TabIndex = 138;
            this.btn_delete.Text = "حذف عضو";
            this.btn_delete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_edit
            // 
            this.btn_edit.Image = global::LibrarySoft.Properties.Resources.edit;
            this.btn_edit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_edit.Location = new System.Drawing.Point(640, 557);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(105, 43);
            this.btn_edit.TabIndex = 137;
            this.btn_edit.Text = "ویرایش عضو";
            this.btn_edit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_edit.UseVisualStyleBackColor = true;
            this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
            // 
            // btn_save
            // 
            this.btn_save.Image = global::LibrarySoft.Properties.Resources.Save_Icon;
            this.btn_save.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_save.Location = new System.Drawing.Point(744, 557);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(105, 43);
            this.btn_save.TabIndex = 14;
            this.btn_save.Text = "ثبت عضو";
            this.btn_save.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_new
            // 
            this.btn_new.Image = global::LibrarySoft.Properties.Resources.select;
            this.btn_new.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_new.Location = new System.Drawing.Point(848, 557);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(105, 43);
            this.btn_new.TabIndex = 15;
            this.btn_new.Text = "عضو جدید";
            this.btn_new.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_new.UseVisualStyleBackColor = true;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::LibrarySoft.Properties.Resources.search;
            this.pictureBox1.Location = new System.Drawing.Point(922, 302);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 23);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 70;
            this.pictureBox1.TabStop = false;
            // 
            // Member_Frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(956, 602);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_address);
            this.Controls.Add(this.btn_list_book);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_edit);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.btn_new);
            this.Controls.Add(this.com_madrak);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txt_email);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txt_tel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txt_code_meli);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txt_pname);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_lname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_fname);
            this.Controls.Add(this.lbl_count);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_shenas);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txt_search);
            this.Controls.Add(this.Gridview_Member);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Member_Frm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فرم اطلاعات اعضاء";
            this.Load += new System.EventHandler(this.Member_Frm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Gridview_Member)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Gridview_Member;
        private System.Windows.Forms.TextBox txt_search;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_shenas;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbl_count;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_fname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_lname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_pname;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_code_meli;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txt_modat;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txt_email;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txt_tel;
        private System.Windows.Forms.MaskedTextBox txt_sdate;
        private System.Windows.Forms.ComboBox com_madrak;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_edit;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Button btn_list_book;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_address;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox txt_edate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txt_price;
    }
}