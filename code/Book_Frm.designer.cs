﻿namespace LibrarySoft
{
    partial class Book_Frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Gridview_Book = new System.Windows.Forms.DataGridView();
            this.txt_search = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_shenas = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lbl_count = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_name_book = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_subject = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_auther = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txt_count = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txt_price = new System.Windows.Forms.TextBox();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_edit = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_new = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_publisher = new System.Windows.Forms.TextBox();
            this.txt_sal = new System.Windows.Forms.TextBox();
            this.com_type = new System.Windows.Forms.ComboBox();
            this.btn_list_member = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Gridview_Book)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Gridview_Book
            // 
            this.Gridview_Book.AllowUserToAddRows = false;
            this.Gridview_Book.AllowUserToDeleteRows = false;
            this.Gridview_Book.AllowUserToResizeColumns = false;
            this.Gridview_Book.AllowUserToResizeRows = false;
            this.Gridview_Book.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Gridview_Book.ColumnHeadersHeight = 37;
            this.Gridview_Book.Location = new System.Drawing.Point(4, 333);
            this.Gridview_Book.Name = "Gridview_Book";
            this.Gridview_Book.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Gridview_Book.Size = new System.Drawing.Size(948, 215);
            this.Gridview_Book.TabIndex = 14;
            this.Gridview_Book.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.Gridview_Book_CellFormatting);
            this.Gridview_Book.Click += new System.EventHandler(this.Gridview_Book_Click);
            this.Gridview_Book.DoubleClick += new System.EventHandler(this.Gridview_Book_DoubleClick);
            this.Gridview_Book.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Gridview_Book_KeyDown);
            this.Gridview_Book.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Gridview_Book_KeyUp);
            // 
            // txt_search
            // 
            this.txt_search.BackColor = System.Drawing.Color.Gainsboro;
            this.txt_search.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_search.Location = new System.Drawing.Point(4, 304);
            this.txt_search.Name = "txt_search";
            this.txt_search.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_search.Size = new System.Drawing.Size(912, 27);
            this.txt_search.TabIndex = 13;
            this.txt_search.TextChanged += new System.EventHandler(this.txt_search_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(876, 13);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(74, 20);
            this.label4.TabIndex = 95;
            this.label4.Text = "شناسه کتاب :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_shenas
            // 
            this.txt_shenas.BackColor = System.Drawing.Color.LightCyan;
            this.txt_shenas.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_shenas.Location = new System.Drawing.Point(690, 10);
            this.txt_shenas.Name = "txt_shenas";
            this.txt_shenas.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_shenas.Size = new System.Drawing.Size(157, 27);
            this.txt_shenas.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label9.ForeColor = System.Drawing.Color.Crimson;
            this.label9.Location = new System.Drawing.Point(45, 570);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(114, 20);
            this.label9.TabIndex = 102;
            this.label9.Text = "تعداد کتاب ثبت شده :";
            // 
            // lbl_count
            // 
            this.lbl_count.AutoSize = true;
            this.lbl_count.Font = new System.Drawing.Font("B Yekan", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lbl_count.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lbl_count.Location = new System.Drawing.Point(9, 565);
            this.lbl_count.Name = "lbl_count";
            this.lbl_count.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_count.Size = new System.Drawing.Size(23, 27);
            this.lbl_count.TabIndex = 103;
            this.lbl_count.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(894, 44);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(56, 20);
            this.label2.TabIndex = 109;
            this.label2.Text = "نام کتاب :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_name_book
            // 
            this.txt_name_book.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_name_book.Location = new System.Drawing.Point(347, 41);
            this.txt_name_book.MaxLength = 0;
            this.txt_name_book.Name = "txt_name_book";
            this.txt_name_book.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_name_book.Size = new System.Drawing.Size(500, 27);
            this.txt_name_book.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(874, 75);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(76, 20);
            this.label3.TabIndex = 111;
            this.label3.Text = "موضوع کتاب :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_subject
            // 
            this.txt_subject.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_subject.Location = new System.Drawing.Point(690, 72);
            this.txt_subject.Name = "txt_subject";
            this.txt_subject.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_subject.Size = new System.Drawing.Size(157, 27);
            this.txt_subject.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(880, 106);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(70, 20);
            this.label5.TabIndex = 113;
            this.label5.Text = "عنوان کتاب :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(888, 200);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(62, 20);
            this.label6.TabIndex = 115;
            this.label6.Text = "انتشارات :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(868, 138);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(82, 20);
            this.label7.TabIndex = 117;
            this.label7.Text = "نویسنده کتاب :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_auther
            // 
            this.txt_auther.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_auther.Location = new System.Drawing.Point(347, 135);
            this.txt_auther.MaxLength = 0;
            this.txt_auther.Name = "txt_auther";
            this.txt_auther.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_auther.Size = new System.Drawing.Size(500, 27);
            this.txt_auther.TabIndex = 4;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(849, 262);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(101, 20);
            this.label14.TabIndex = 129;
            this.label14.Text = "تعداد کتاب موجود :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_count
            // 
            this.txt_count.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_count.Location = new System.Drawing.Point(797, 259);
            this.txt_count.Name = "txt_count";
            this.txt_count.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_count.Size = new System.Drawing.Size(50, 27);
            this.txt_count.TabIndex = 8;
            this.txt_count.Text = "0";
            this.txt_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(878, 169);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(72, 20);
            this.label8.TabIndex = 129;
            this.label8.Text = "سال انتشار :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(888, 232);
            this.label17.Name = "label17";
            this.label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label17.Size = new System.Drawing.Size(62, 20);
            this.label17.TabIndex = 144;
            this.label17.Text = "مبلغ کتاب :";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_price
            // 
            this.txt_price.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_price.Location = new System.Drawing.Point(690, 229);
            this.txt_price.Name = "txt_price";
            this.txt_price.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_price.Size = new System.Drawing.Size(157, 27);
            this.txt_price.TabIndex = 7;
            this.txt_price.Text = "0";
            this.txt_price.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Image = global::LibrarySoft.Properties.Resources.cancel;
            this.btn_cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cancel.Location = new System.Drawing.Point(423, 556);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(106, 43);
            this.btn_cancel.TabIndex = 139;
            this.btn_cancel.Text = "انصراف";
            this.btn_cancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.Image = global::LibrarySoft.Properties.Resources.delete;
            this.btn_delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_delete.Location = new System.Drawing.Point(529, 556);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(106, 43);
            this.btn_delete.TabIndex = 138;
            this.btn_delete.Text = "حذف کتاب";
            this.btn_delete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_edit
            // 
            this.btn_edit.Image = global::LibrarySoft.Properties.Resources.edit;
            this.btn_edit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_edit.Location = new System.Drawing.Point(635, 556);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(106, 43);
            this.btn_edit.TabIndex = 137;
            this.btn_edit.Text = "ویرایش کتاب";
            this.btn_edit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_edit.UseVisualStyleBackColor = true;
            this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
            // 
            // btn_save
            // 
            this.btn_save.Image = global::LibrarySoft.Properties.Resources.Save_Icon;
            this.btn_save.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_save.Location = new System.Drawing.Point(741, 556);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(106, 43);
            this.btn_save.TabIndex = 9;
            this.btn_save.Text = "ثبت کتاب";
            this.btn_save.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_new
            // 
            this.btn_new.Image = global::LibrarySoft.Properties.Resources.select;
            this.btn_new.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_new.Location = new System.Drawing.Point(847, 556);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(106, 43);
            this.btn_new.TabIndex = 12;
            this.btn_new.Text = "کتاب جدید";
            this.btn_new.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_new.UseVisualStyleBackColor = true;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::LibrarySoft.Properties.Resources.search;
            this.pictureBox1.Location = new System.Drawing.Point(922, 304);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 23);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 70;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(654, 232);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(31, 20);
            this.label1.TabIndex = 145;
            this.label1.Text = "ریال";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_publisher
            // 
            this.txt_publisher.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_publisher.Location = new System.Drawing.Point(347, 198);
            this.txt_publisher.MaxLength = 0;
            this.txt_publisher.Name = "txt_publisher";
            this.txt_publisher.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_publisher.Size = new System.Drawing.Size(500, 27);
            this.txt_publisher.TabIndex = 6;
            // 
            // txt_sal
            // 
            this.txt_sal.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_sal.Location = new System.Drawing.Point(797, 166);
            this.txt_sal.MaxLength = 4;
            this.txt_sal.Name = "txt_sal";
            this.txt_sal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_sal.Size = new System.Drawing.Size(50, 27);
            this.txt_sal.TabIndex = 5;
            this.txt_sal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // com_type
            // 
            this.com_type.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.com_type.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.com_type.Font = new System.Drawing.Font("B Yekan", 9.75F);
            this.com_type.FormattingEnabled = true;
            this.com_type.Items.AddRange(new object[] {
            "علمی",
            "پژوهشی",
            "فنی و مهندسی",
            "آموزشی",
            "ورزشی",
            "هنری",
            "سیاسی",
            "اقتصادی",
            "ظنز",
            "تاریخی",
            "جفرافیا",
            "ادبیات و داستان",
            "پزشکی",
            "حقوق",
            "سایر"});
            this.com_type.Location = new System.Drawing.Point(690, 103);
            this.com_type.Name = "com_type";
            this.com_type.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.com_type.Size = new System.Drawing.Size(157, 28);
            this.com_type.TabIndex = 3;
            // 
            // btn_list_member
            // 
            this.btn_list_member.Image = global::LibrarySoft.Properties.Resources.ListUser;
            this.btn_list_member.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_list_member.Location = new System.Drawing.Point(185, 556);
            this.btn_list_member.Name = "btn_list_member";
            this.btn_list_member.Size = new System.Drawing.Size(238, 43);
            this.btn_list_member.TabIndex = 146;
            this.btn_list_member.Text = "لیست اعضایی که کتاب را به امانت گرفتند";
            this.btn_list_member.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_list_member.UseVisualStyleBackColor = true;
            this.btn_list_member.Click += new System.EventHandler(this.btn_list_member_Click);
            // 
            // Book_Frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(956, 602);
            this.Controls.Add(this.btn_list_member);
            this.Controls.Add(this.com_type);
            this.Controls.Add(this.txt_sal);
            this.Controls.Add(this.txt_publisher);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txt_price);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_edit);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.btn_new);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txt_count);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txt_auther);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_subject);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_name_book);
            this.Controls.Add(this.lbl_count);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_shenas);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txt_search);
            this.Controls.Add(this.Gridview_Book);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Book_Frm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فرم اطلاعات کتاب ها";
            this.Load += new System.EventHandler(this.Book_Frm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Gridview_Book)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Gridview_Book;
        private System.Windows.Forms.TextBox txt_search;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_shenas;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbl_count;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_name_book;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_subject;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_auther;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txt_count;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_edit;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txt_price;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_publisher;
        private System.Windows.Forms.TextBox txt_sal;
        private System.Windows.Forms.ComboBox com_type;
        private System.Windows.Forms.Button btn_list_member;
    }
}