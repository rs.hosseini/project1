﻿namespace LibrarySoft
{
    partial class SearchMember_Frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Gridview_Member = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txt_search = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Gridview_Member)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Gridview_Member
            // 
            this.Gridview_Member.AllowUserToAddRows = false;
            this.Gridview_Member.AllowUserToDeleteRows = false;
            this.Gridview_Member.AllowUserToResizeColumns = false;
            this.Gridview_Member.AllowUserToResizeRows = false;
            this.Gridview_Member.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Gridview_Member.ColumnHeadersHeight = 37;
            this.Gridview_Member.Location = new System.Drawing.Point(4, 41);
            this.Gridview_Member.Name = "Gridview_Member";
            this.Gridview_Member.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Gridview_Member.Size = new System.Drawing.Size(471, 443);
            this.Gridview_Member.TabIndex = 15;
            this.Gridview_Member.DoubleClick += new System.EventHandler(this.Gridview_Member_DoubleClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::LibrarySoft.Properties.Resources.search;
            this.pictureBox1.Location = new System.Drawing.Point(447, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 23);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 71;
            this.pictureBox1.TabStop = false;
            // 
            // txt_search
            // 
            this.txt_search.BackColor = System.Drawing.Color.Gainsboro;
            this.txt_search.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_search.Location = new System.Drawing.Point(4, 7);
            this.txt_search.Name = "txt_search";
            this.txt_search.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_search.Size = new System.Drawing.Size(436, 27);
            this.txt_search.TabIndex = 72;
            this.txt_search.TextChanged += new System.EventHandler(this.txt_search_TextChanged);
            // 
            // SearchMember_Frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(480, 490);
            this.Controls.Add(this.txt_search);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Gridview_Member);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "SearchMember_Frm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "جستجو اعضاء کتابخانه";
            this.Load += new System.EventHandler(this.SearchMember_Frm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Gridview_Member)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Gridview_Member;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txt_search;
    }
}