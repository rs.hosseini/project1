﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LibrarySoft
{
    public partial class Book_Frm : Form
    {
        Class_DBS dbs = new Class_DBS();
        DataTable dt = new DataTable();
        public string id;

        public Book_Frm()
        {
            InitializeComponent();
        }

        public void CountRows()
        {
            lbl_count.Text = Gridview_Book.Rows.Count.ToString();
        }
        //...تابع شمردن تعداد رکوردهای ثبت شده 

        public void HeaderEdit()
        {
            Gridview_Book.Columns[1].HeaderText = "شناسه کتاب";
            Gridview_Book.Columns[2].HeaderText = "نام کتاب";
            Gridview_Book.Columns[3].HeaderText = "موضوع کتاب";
            Gridview_Book.Columns[4].HeaderText = "عنوان کتاب";
            Gridview_Book.Columns[5].HeaderText = "نویسنده";
            Gridview_Book.Columns[6].HeaderText = "سال انتشار";
            Gridview_Book.Columns[7].HeaderText = "انتشارات";
            Gridview_Book.Columns[8].HeaderText = "مبلغ کتاب";
            Gridview_Book.Columns[9].HeaderText = "تعداد کتاب موجود";

            Gridview_Book.Columns[1].Width = 65;
            Gridview_Book.Columns[2].Width = 160;
            Gridview_Book.Columns[3].Width = 125;
            Gridview_Book.Columns[4].Width = 125;
            Gridview_Book.Columns[5].Width = 112;
            Gridview_Book.Columns[6].Width = 50;
            Gridview_Book.Columns[7].Width = 110;
            Gridview_Book.Columns[8].Width = 80;
            Gridview_Book.Columns[9].Width = 60;

            Gridview_Book.Columns[0].Visible = false;
        }
        //...تابع ویرایش بخش هدر گریدویو

        public void EBtn()
        {
            btn_new.Enabled = true;
            btn_save.Enabled = false;
            btn_edit.Enabled = true;
            btn_delete.Enabled = true;
        }

        public void DBtn()
        {
            btn_new.Enabled = false;
            btn_save.Enabled = true;
            btn_edit.Enabled = false;
            btn_delete.Enabled = false;
        }

        public void FullGrid_Book()
        {
            dbs.connect();
            Gridview_Book.DataSource = dbs.show("Select * From BookInfo");
            dbs.disconnect();
            HeaderEdit();
        }

        public void Clear()
        {
            txt_auther.Text = null;
            txt_count.Text = "0";
            txt_name_book.Text = null;
            txt_price.Text = null;
            txt_publisher.Text = null;
            txt_sal.Text = null;
            txt_shenas.Text = null;
            txt_subject.Text = null;
            com_type.Text = null;
        }

        public void IndexGridView()
        {
            id = Gridview_Book[0, Gridview_Book.CurrentRow.Index].Value.ToString();
            txt_shenas.Text = Gridview_Book[1, Gridview_Book.CurrentRow.Index].Value.ToString();
            txt_name_book.Text = Gridview_Book[2, Gridview_Book.CurrentRow.Index].Value.ToString();
            txt_subject.Text = Gridview_Book[3, Gridview_Book.CurrentRow.Index].Value.ToString();
            com_type.Text = Gridview_Book[4, Gridview_Book.CurrentRow.Index].Value.ToString();
            txt_auther.Text = Gridview_Book[5, Gridview_Book.CurrentRow.Index].Value.ToString();
            txt_sal.Text = Gridview_Book[6, Gridview_Book.CurrentRow.Index].Value.ToString();
            txt_publisher.Text = Gridview_Book[7, Gridview_Book.CurrentRow.Index].Value.ToString();
            txt_price.Text = Gridview_Book[8, Gridview_Book.CurrentRow.Index].Value.ToString();
            txt_count.Text = Gridview_Book[9, Gridview_Book.CurrentRow.Index].Value.ToString();
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            txt_shenas.Focus();
            Clear();
            dbs.disconnect();
            FullGrid_Book();
            DBtn();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            dbs.connect();
            dt = dbs.show("Select * From BookInfo Where shenas_book='" + txt_shenas.Text + "'");
            if (dt.Rows.Count > 0)
            {
                MessageBox.Show("کاربر محترم شناسه کتاب تکراری می باشد", "اخطار", MessageBoxButtons.OK, MessageBoxIcon.Error);
                dbs.disconnect();
            }
            else
            {
                dbs.execute("Insert Into BookInfo Values('" + txt_shenas.Text + "',N'" + txt_name_book.Text + "',N'" + txt_subject.Text + "',N'" + com_type.Text + "',N'" + txt_auther.Text + "','" + txt_sal.Text + "',N'" + txt_publisher.Text + "','" + txt_price.Text + "','" + txt_count.Text + "')");
                dbs.disconnect();
                FullGrid_Book();
                EBtn();
                Clear();
                CountRows();
                MessageBox.Show("کاربر محترم عملیات ثبت کتاب با موفقیت انجام شد", "پیغام", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btn_edit_Click(object sender, EventArgs e)
        {
            dbs.connect();
            dbs.execute("Update BookInfo Set shenas_book='" + txt_shenas.Text + "',name_book=N'" + txt_name_book.Text + "',subject_book=N'" + txt_subject.Text + "',type_book=N'" + com_type.Text + "',auther=N'" + txt_auther.Text + "',sal_publish='" + txt_sal.Text + "',publisher=N'" + txt_publisher.Text + "',price='" + txt_price.Text + "',count_book='" + txt_count.Text + "' Where ID='" + id + "'");
            dbs.disconnect();
            FullGrid_Book();
            EBtn();
            Clear();
            MessageBox.Show("کاربر محترم عملیات ثبت کتاب با موفقیت انجام شد", "پیغام", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("کاربر محترم آیا مطمئن به حذف کتاب مورد نظر می باشید؟", "هشدار", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                dbs.connect();
                dbs.execute("Delete From BookInfo Where ID='" + id + "'");
                dbs.disconnect();
                FullGrid_Book();
                EBtn();
                CountRows();
                Clear();
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Clear();
            dbs.disconnect();
            FullGrid_Book();
            EBtn();
        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            dbs.connect();
            Gridview_Book.DataSource = dbs.show("Select * From BookInfo Where shenas_book Like '%" + txt_search.Text + "%' or name_book Like N'%" + txt_search.Text + "%' or subject_book Like N'%" + txt_search.Text + "%' or type_book Like N'%" + txt_search.Text + "%' or auther Like N'%" + txt_search.Text + "%'");
            dbs.disconnect();
            HeaderEdit();
            CountRows();
        }

        private void Book_Frm_Load(object sender, EventArgs e)
        {
            FullGrid_Book();
            CountRows();
        }

        private void Gridview_Book_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 8 && e.RowIndex != this.Gridview_Book.NewRowIndex)
                {
                    double d = double.Parse(e.Value.ToString());
                    e.Value = d.ToString("#,##0.##");
                }

            }
            catch (Exception ex)
            {
            }
        }

        private void Gridview_Book_Click(object sender, EventArgs e)
        {
            IndexGridView();
        }

        private void Gridview_Book_KeyDown(object sender, KeyEventArgs e)
        {
            IndexGridView();
        }

        private void Gridview_Book_KeyUp(object sender, KeyEventArgs e)
        {
            IndexGridView();
        }

        private void btn_list_member_Click(object sender, EventArgs e)
        {
            ListBorrowMember_Frm lbmf = new ListBorrowMember_Frm();
            lbmf.sh_book = txt_shenas.Text;
            lbmf.ShowDialog();
        }

        private void Gridview_Book_DoubleClick(object sender, EventArgs e)
        {
            btn_list_member_Click(sender, e);
        }

    }
}
