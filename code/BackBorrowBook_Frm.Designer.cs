﻿namespace LibrarySoft
{
    partial class BackBorrowBook_Frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_search = new System.Windows.Forms.TextBox();
            this.Gridview_Borrow = new System.Windows.Forms.DataGridView();
            this.lbl_count = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_sdate = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_modat = new System.Windows.Forms.TextBox();
            this.txt_edate = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbl_price = new System.Windows.Forms.Label();
            this.lbl_day = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btn_back_borrow = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Gridview_Borrow)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_search
            // 
            this.txt_search.BackColor = System.Drawing.Color.Gainsboro;
            this.txt_search.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_search.Location = new System.Drawing.Point(6, 130);
            this.txt_search.Name = "txt_search";
            this.txt_search.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_search.Size = new System.Drawing.Size(894, 27);
            this.txt_search.TabIndex = 102;
            this.txt_search.TextChanged += new System.EventHandler(this.txt_search_TextChanged);
            // 
            // Gridview_Borrow
            // 
            this.Gridview_Borrow.AllowUserToAddRows = false;
            this.Gridview_Borrow.AllowUserToDeleteRows = false;
            this.Gridview_Borrow.AllowUserToResizeColumns = false;
            this.Gridview_Borrow.AllowUserToResizeRows = false;
            this.Gridview_Borrow.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Gridview_Borrow.ColumnHeadersHeight = 37;
            this.Gridview_Borrow.Location = new System.Drawing.Point(6, 163);
            this.Gridview_Borrow.Name = "Gridview_Borrow";
            this.Gridview_Borrow.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Gridview_Borrow.Size = new System.Drawing.Size(934, 362);
            this.Gridview_Borrow.TabIndex = 103;
            this.Gridview_Borrow.Click += new System.EventHandler(this.Gridview_Borrow_Click);
            this.Gridview_Borrow.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Gridview_Borrow_KeyDown);
            this.Gridview_Borrow.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Gridview_Borrow_KeyUp);
            // 
            // lbl_count
            // 
            this.lbl_count.AutoSize = true;
            this.lbl_count.Font = new System.Drawing.Font("B Yekan", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lbl_count.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lbl_count.Location = new System.Drawing.Point(9, 557);
            this.lbl_count.Name = "lbl_count";
            this.lbl_count.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_count.Size = new System.Drawing.Size(23, 27);
            this.lbl_count.TabIndex = 146;
            this.lbl_count.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label9.ForeColor = System.Drawing.Color.Crimson;
            this.label9.Location = new System.Drawing.Point(40, 562);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(167, 20);
            this.label9.TabIndex = 145;
            this.label9.Text = "تعداد کتاب های امانت داده شده :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(759, 78);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(27, 20);
            this.label12.TabIndex = 151;
            this.label12.Text = "روز";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(867, 79);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(68, 20);
            this.label10.TabIndex = 150;
            this.label10.Text = "مدت امانت :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_sdate
            // 
            this.txt_sdate.BackColor = System.Drawing.Color.White;
            this.txt_sdate.Font = new System.Drawing.Font("B Yekan", 9.75F);
            this.txt_sdate.Location = new System.Drawing.Point(717, 12);
            this.txt_sdate.Mask = "####/##/##";
            this.txt_sdate.Name = "txt_sdate";
            this.txt_sdate.Size = new System.Drawing.Size(115, 27);
            this.txt_sdate.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(834, 15);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(101, 20);
            this.label8.TabIndex = 149;
            this.label8.Text = "تاریخ شروع امانت :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_modat
            // 
            this.txt_modat.BackColor = System.Drawing.Color.Gold;
            this.txt_modat.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txt_modat.Location = new System.Drawing.Point(786, 75);
            this.txt_modat.Name = "txt_modat";
            this.txt_modat.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_modat.Size = new System.Drawing.Size(46, 27);
            this.txt_modat.TabIndex = 4;
            this.txt_modat.Text = "0";
            this.txt_modat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_edate
            // 
            this.txt_edate.BackColor = System.Drawing.Color.White;
            this.txt_edate.Font = new System.Drawing.Font("B Yekan", 9.75F);
            this.txt_edate.Location = new System.Drawing.Point(717, 44);
            this.txt_edate.Mask = "####/##/##";
            this.txt_edate.Name = "txt_edate";
            this.txt_edate.Size = new System.Drawing.Size(115, 27);
            this.txt_edate.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(840, 47);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(95, 20);
            this.label2.TabIndex = 153;
            this.label2.Text = "تاریخ پایان امانت :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lbl_price);
            this.groupBox1.Controls.Add(this.lbl_day);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.groupBox1.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.groupBox1.Location = new System.Drawing.Point(6, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox1.Size = new System.Drawing.Size(272, 100);
            this.groupBox1.TabIndex = 154;
            this.groupBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label6.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label6.Location = new System.Drawing.Point(89, 65);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(28, 20);
            this.label6.TabIndex = 133;
            this.label6.Text = "عدد";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label5.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label5.Location = new System.Drawing.Point(98, 24);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(31, 20);
            this.label5.TabIndex = 132;
            this.label5.Text = "ریال";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl_price
            // 
            this.lbl_price.AutoSize = true;
            this.lbl_price.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lbl_price.ForeColor = System.Drawing.Color.ForestGreen;
            this.lbl_price.Location = new System.Drawing.Point(147, 24);
            this.lbl_price.Name = "lbl_price";
            this.lbl_price.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_price.Size = new System.Drawing.Size(17, 20);
            this.lbl_price.TabIndex = 131;
            this.lbl_price.Text = "0";
            this.lbl_price.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_day
            // 
            this.lbl_day.AutoSize = true;
            this.lbl_day.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lbl_day.ForeColor = System.Drawing.Color.ForestGreen;
            this.lbl_day.Location = new System.Drawing.Point(121, 65);
            this.lbl_day.Name = "lbl_day";
            this.lbl_day.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lbl_day.Size = new System.Drawing.Size(17, 20);
            this.lbl_day.TabIndex = 130;
            this.lbl_day.Text = "0";
            this.lbl_day.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label4.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label4.Location = new System.Drawing.Point(148, 65);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(114, 20);
            this.label4.TabIndex = 129;
            this.label4.Text = "تعداد روزهای جریمه :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("B Yekan", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label13.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label13.Location = new System.Drawing.Point(193, 24);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(69, 20);
            this.label13.TabIndex = 127;
            this.label13.Text = "مبلغ جریمه :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btn_back_borrow
            // 
            this.btn_back_borrow.Image = global::LibrarySoft.Properties.Resources.back_borrow;
            this.btn_back_borrow.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_back_borrow.Location = new System.Drawing.Point(785, 536);
            this.btn_back_borrow.Name = "btn_back_borrow";
            this.btn_back_borrow.Size = new System.Drawing.Size(155, 55);
            this.btn_back_borrow.TabIndex = 141;
            this.btn_back_borrow.Text = "برگشت از امانت";
            this.btn_back_borrow.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_back_borrow.UseVisualStyleBackColor = true;
            this.btn_back_borrow.Click += new System.EventHandler(this.btn_back_borrow_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::LibrarySoft.Properties.Resources.search;
            this.pictureBox1.Location = new System.Drawing.Point(909, 130);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 23);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 104;
            this.pictureBox1.TabStop = false;
            // 
            // BackBorrowBook_Frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(945, 596);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txt_edate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txt_sdate);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txt_modat);
            this.Controls.Add(this.lbl_count);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btn_back_borrow);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txt_search);
            this.Controls.Add(this.Gridview_Borrow);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "BackBorrowBook_Frm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فرم برگشت از امانت";
            this.Load += new System.EventHandler(this.BackBorrowBook_Frm_Load);
            this.Shown += new System.EventHandler(this.BackBorrowBook_Frm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.Gridview_Borrow)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txt_search;
        private System.Windows.Forms.DataGridView Gridview_Borrow;
        private System.Windows.Forms.Button btn_back_borrow;
        private System.Windows.Forms.Label lbl_count;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox txt_sdate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_modat;
        private System.Windows.Forms.MaskedTextBox txt_edate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbl_price;
        private System.Windows.Forms.Label lbl_day;
    }
}