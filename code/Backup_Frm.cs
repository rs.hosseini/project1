﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace LibrarySoft
{
    public partial class Backup_Frm : Form
    {
        public Backup_Frm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txt_patch.Text == "")
                MessageBox.Show("لطفا مسیر فایل را انتخاب کنید", "اخطار", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {                
                string backup_query;

                backup_query = "Backup Database Library To Disk='" + @txt_patch.Text + "'";
                SqlConnection con = new SqlConnection("Data Source=.;Initial Catalog=Library;Integrated Security=True");
                con.Open();

                SqlCommand com = new SqlCommand(backup_query, con);
                com.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("عملیات با موفقیت انجام شد", "پیغام", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folder_open = new FolderBrowserDialog();
            folder_open.ShowDialog();
            txt_patch.Text = folder_open.SelectedPath + @"\LibrarySoft.txt";
        }

        private void Backup_Frm_Load(object sender, EventArgs e)
        {

        }
    }
}
