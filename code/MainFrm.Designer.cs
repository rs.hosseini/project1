﻿namespace LibrarySoft
{
    partial class MainFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFrm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.اطلاعاتپایهToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_member = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_book = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_persoenl = new System.Windows.Forms.ToolStripMenuItem();
            this.بخشاماناتToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_amanat = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_back_amanat = new System.Windows.Forms.ToolStripMenuItem();
            this.تنظیماتنرمافزارToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_backup = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.امکاناتنرمافزارToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.BackColor = System.Drawing.Color.Gold;
            this.menuStrip1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.اطلاعاتپایهToolStripMenuItem,
            this.بخشاماناتToolStripMenuItem,
            this.تنظیماتنرمافزارToolStripMenuItem,
            this.امکاناتنرمافزارToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.menuStrip1.Size = new System.Drawing.Size(1014, 40);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // اطلاعاتپایهToolStripMenuItem
            // 
            this.اطلاعاتپایهToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_member,
            this.toolStripMenuItem1,
            this.btn_book,
            this.toolStripMenuItem2,
            this.btn_persoenl});
            this.اطلاعاتپایهToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.اطلاعاتپایهToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.اطلاعاتپایهToolStripMenuItem.Name = "اطلاعاتپایهToolStripMenuItem";
            this.اطلاعاتپایهToolStripMenuItem.Size = new System.Drawing.Size(80, 36);
            this.اطلاعاتپایهToolStripMenuItem.Text = "اطلاعات پایه";
            // 
            // btn_member
            // 
            this.btn_member.Image = global::LibrarySoft.Properties.Resources.member;
            this.btn_member.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btn_member.Name = "btn_member";
            this.btn_member.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.btn_member.Size = new System.Drawing.Size(227, 42);
            this.btn_member.Text = "اطلاعات اعضاء";
            this.btn_member.Click += new System.EventHandler(this.btn_member_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(224, 6);
            // 
            // btn_book
            // 
            this.btn_book.Image = global::LibrarySoft.Properties.Resources.book;
            this.btn_book.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btn_book.Name = "btn_book";
            this.btn_book.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this.btn_book.Size = new System.Drawing.Size(227, 42);
            this.btn_book.Text = "اطلاعات کتاب ها";
            this.btn_book.Click += new System.EventHandler(this.btn_book_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(224, 6);
            // 
            // btn_persoenl
            // 
            this.btn_persoenl.Name = "btn_persoenl";
            this.btn_persoenl.Size = new System.Drawing.Size(227, 42);
            // 
            // بخشاماناتToolStripMenuItem
            // 
            this.بخشاماناتToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_amanat,
            this.toolStripMenuItem3,
            this.btn_back_amanat});
            this.بخشاماناتToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.بخشاماناتToolStripMenuItem.Name = "بخشاماناتToolStripMenuItem";
            this.بخشاماناتToolStripMenuItem.Size = new System.Drawing.Size(81, 36);
            this.بخشاماناتToolStripMenuItem.Text = "بخش امانات";
            // 
            // btn_amanat
            // 
            this.btn_amanat.Image = global::LibrarySoft.Properties.Resources.borrow_book;
            this.btn_amanat.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btn_amanat.Name = "btn_amanat";
            this.btn_amanat.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.btn_amanat.Size = new System.Drawing.Size(251, 42);
            this.btn_amanat.Text = "به امانت بردن کتاب";
            this.btn_amanat.Click += new System.EventHandler(this.btn_amanat_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(248, 6);
            // 
            // btn_back_amanat
            // 
            this.btn_back_amanat.Image = global::LibrarySoft.Properties.Resources.back_borrow_member;
            this.btn_back_amanat.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btn_back_amanat.Name = "btn_back_amanat";
            this.btn_back_amanat.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.btn_back_amanat.Size = new System.Drawing.Size(251, 42);
            this.btn_back_amanat.Text = "برگشت از امانت کتاب";
            this.btn_back_amanat.Click += new System.EventHandler(this.btn_back_amanat_Click);
            // 
            // تنظیماتنرمافزارToolStripMenuItem
            // 
            this.تنظیماتنرمافزارToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_backup,
            this.toolStripMenuItem4});
            this.تنظیماتنرمافزارToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.تنظیماتنرمافزارToolStripMenuItem.Name = "تنظیماتنرمافزارToolStripMenuItem";
            this.تنظیماتنرمافزارToolStripMenuItem.Size = new System.Drawing.Size(60, 36);
            this.تنظیماتنرمافزارToolStripMenuItem.Text = "امکانات";
            this.تنظیماتنرمافزارToolStripMenuItem.Click += new System.EventHandler(this.امکاناتToolStripMenuItem_Click);
            // 
            // btn_backup
            // 
            this.btn_backup.Image = global::LibrarySoft.Properties.Resources.backup;
            this.btn_backup.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btn_backup.Name = "btn_backup";
            this.btn_backup.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.btn_backup.Size = new System.Drawing.Size(232, 42);
            this.btn_backup.Text = "گزارش گیری";
            this.btn_backup.Click += new System.EventHandler(this.btn_backup_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(229, 6);
            // 
            // امکاناتنرمافزارToolStripMenuItem
            // 
            this.امکاناتنرمافزارToolStripMenuItem.Name = "امکاناتنرمافزارToolStripMenuItem";
            this.امکاناتنرمافزارToolStripMenuItem.Size = new System.Drawing.Size(12, 36);
            // 
            // MainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LibrarySoft.Properties.Resources.BackImage;
            this.ClientSize = new System.Drawing.Size(1014, 623);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "نرم افزار مدیریت کتابخانه";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainFrm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem اطلاعاتپایهToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btn_member;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem btn_book;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem btn_persoenl;
        private System.Windows.Forms.ToolStripMenuItem بخشاماناتToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btn_amanat;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem btn_back_amanat;
        private System.Windows.Forms.ToolStripMenuItem تنظیماتنرمافزارToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btn_backup;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem امکاناتنرمافزارToolStripMenuItem;
    }
}

