﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace LibrarySoft
{
    public partial class BackBorrowBook_Frm : Form
    {
        public string id, sh_member, sh_book, date1, date2, today, madrak;
        public int price, day, num_date1, num_date2;

        Class_DBS dbs = new Class_DBS();
        DataTable dt = new DataTable();
        public Boolean state;

        public void ShamsiDate()
        {
            PersianCalendar pdate = new PersianCalendar();
            today = pdate.GetYear(DateTime.Now).ToString() + "/" + pdate.GetMonth(DateTime.Now).ToString().PadLeft(2, '0') + "/" + pdate.GetDayOfMonth(DateTime.Now).ToString().PadLeft(2, '0');
        }

        public BackBorrowBook_Frm()
        {
            InitializeComponent();
        }

        public void CountRows()
        {
            lbl_count.Text = Gridview_Borrow.Rows.Count.ToString();
        }
        //...تابع شمردن تعداد رکوردهای ثبت شده 

        public void HeaderEdit()
        {
            Gridview_Borrow.Columns[1].HeaderText = "شناسه کتاب";
            Gridview_Borrow.Columns[2].HeaderText = "شناسه عضویت";
            Gridview_Borrow.Columns[3].HeaderText = "نام عضو";
            Gridview_Borrow.Columns[4].HeaderText = "نام خانوادگی";
            Gridview_Borrow.Columns[5].HeaderText = "نام کتاب";
            Gridview_Borrow.Columns[8].HeaderText = "نویسنده";
            Gridview_Borrow.Columns[9].HeaderText = "تاریخ شروع";
            Gridview_Borrow.Columns[10].HeaderText = "تاریخ پایان";
            Gridview_Borrow.Columns[11].HeaderText = "مدت امانت";
            Gridview_Borrow.Columns[12].HeaderText = "روزهای جریمه";
            Gridview_Borrow.Columns[13].HeaderText = "مبلغ جریمه";
            Gridview_Borrow.Columns[14].HeaderText = "وضعیت امانت";

            Gridview_Borrow.Columns[1].Width = 61;
            Gridview_Borrow.Columns[2].Width = 61;
            Gridview_Borrow.Columns[3].Width = 85;
            Gridview_Borrow.Columns[4].Width = 95;
            Gridview_Borrow.Columns[5].Width = 120;
            Gridview_Borrow.Columns[8].Width = 110;
            Gridview_Borrow.Columns[9].Width = 65;
            Gridview_Borrow.Columns[10].Width = 65;
            Gridview_Borrow.Columns[11].Width = 48;
            Gridview_Borrow.Columns[12].Width = 48;
            Gridview_Borrow.Columns[13].Width = 85;
            Gridview_Borrow.Columns[14].Width = 48;

            Gridview_Borrow.Columns[0].Visible = false;
            Gridview_Borrow.Columns[6].Visible = false;
            Gridview_Borrow.Columns[7].Visible = false;
            Gridview_Borrow.Columns[15].Visible = false;
        }
        //...تابع ویرایش بخش هدر گریدویو

        public void FullGrid_Borrow()
        {
            dbs.connect();
            Gridview_Borrow.DataSource = dbs.show("Select * From View_Borrow");
            dbs.disconnect();
            HeaderEdit();
        }

        public void Clear()
        {
            txt_modat.Text = "0";
            txt_sdate.Text = null;
            txt_edate.Text = null;
        }

        public void IndexGridView()
        {
            id = Gridview_Borrow[0, Gridview_Borrow.CurrentRow.Index].Value.ToString();
            txt_sdate.Text = Gridview_Borrow[9, Gridview_Borrow.CurrentRow.Index].Value.ToString();
            txt_edate.Text = Gridview_Borrow[10, Gridview_Borrow.CurrentRow.Index].Value.ToString();
            txt_modat.Text = Gridview_Borrow[11, Gridview_Borrow.CurrentRow.Index].Value.ToString();
            lbl_day.Text = Gridview_Borrow[12, Gridview_Borrow.CurrentRow.Index].Value.ToString();
            lbl_price.Text = Gridview_Borrow[13, Gridview_Borrow.CurrentRow.Index].Value.ToString();
            madrak = Gridview_Borrow[15, Gridview_Borrow.CurrentRow.Index].Value.ToString();
        }

        public void Color_Gridview()
        {
            for (int i = 0; i < Gridview_Borrow.Rows.Count; i++)
            {
                state = Convert.ToBoolean(Gridview_Borrow.Rows[i].Cells[14].Value);
                if (state == true)
                    Gridview_Borrow.Rows[i].DefaultCellStyle.BackColor = Color.PaleGreen;
                else
                    Gridview_Borrow.Rows[i].DefaultCellStyle.BackColor = Color.LightPink;

            }
        }

        public void Account_Price()
        {
            ShamsiDate();
            date1 = txt_sdate.Text;

            num_date1 = (Convert.ToInt32(date1.Substring(0, 4)) * 365) + (Convert.ToInt32(date1.Substring(5, 2)) * 30) + (Convert.ToInt32(date1.Substring(8, 2)));
            num_date2 = (Convert.ToInt32(today.Substring(0, 4)) * 365) + (Convert.ToInt32(today.Substring(5, 2)) * 30) + (Convert.ToInt32(today.Substring(8, 2)));
            day = num_date2 - num_date1;
            if (madrak == "فوق دیپلم")
                lbl_day.Text = (day - 7).ToString();
            else
                if (madrak == "لیسانس")
                    lbl_day.Text = (day - 15).ToString();
                else
                    if (madrak == "فوق لیسانس")
                        lbl_day.Text = (day - 20).ToString();
                    else
                        if (madrak == "دکتری")
                            lbl_day.Text = (day - 30).ToString();

            if ((madrak == "فوق دیپلم") && (day < 7))
                lbl_day.Text = "0";
            else
                if ((madrak == "لیسانس") && (day < 15))
                    lbl_day.Text = "0";
                else
                    if ((madrak == "فوق لیسانس") && (day < 20))
                        lbl_day.Text = "0";
                    else
                        if ((madrak == "دکتری") && (day < 30))
                            lbl_day.Text = "0";


            price = Convert.ToInt32(lbl_day.Text) * 100;
            lbl_price.Text = price.ToString();
        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            dbs.connect();
            Gridview_Borrow.DataSource = dbs.show("Select * From View_Borrow Where shenas_book Like '%" + txt_search.Text + "%' or shenas_member Like '%" + txt_search.Text + "%' or lname Like N'%" + txt_search.Text + "%' or name_book Like N'%" + txt_search.Text + "%' or sdate Like '%" + txt_search.Text + "%'");
            dbs.disconnect();
            HeaderEdit();
            CountRows();
        }

        private void btn_back_borrow_Click(object sender, EventArgs e)
        {
            Account_Price();

            dbs.connect();
            dbs.execute("Update BorrowInfo Set state='" + true + "',day_jarime='" + lbl_day.Text + "',price_jarime='" + lbl_price.Text + "' Where ID='" + id + "'");
            dbs.disconnect();
            FullGrid_Borrow();
            Color_Gridview();
        }

        private void Gridview_Borrow_Click(object sender, EventArgs e)
        {
            IndexGridView();
        }

        private void Gridview_Borrow_KeyDown(object sender, KeyEventArgs e)
        {
            IndexGridView();
        }

        private void Gridview_Borrow_KeyUp(object sender, KeyEventArgs e)
        {
            IndexGridView();
        }

        private void BackBorrowBook_Frm_Load(object sender, EventArgs e)
        {
            FullGrid_Borrow();

        }

        private void BackBorrowBook_Frm_Shown(object sender, EventArgs e)
        {
            Color_Gridview();
        }

    }
}
