﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LibrarySoft
{
    public partial class Member_Frm : Form
    {
        Class_DBS dbs = new Class_DBS();
        DataTable dt = new DataTable();
        public string id;

        public Member_Frm()
        {
            InitializeComponent();
        }

        public void CountRows()
        {
            lbl_count.Text = Gridview_Member.Rows.Count.ToString();
        }
        //...تابع شمردن تعداد رکوردهای ثبت شده 

        public void HeaderEdit()
        {
            Gridview_Member.Columns[1].HeaderText = "شماره عضویت";
            Gridview_Member.Columns[2].HeaderText = "نام";
            Gridview_Member.Columns[3].HeaderText = "نام خانوادگی";
            Gridview_Member.Columns[4].HeaderText = "نام پدر";
            Gridview_Member.Columns[5].HeaderText = "مدرک تحصیلی";
            Gridview_Member.Columns[6].HeaderText = "کد ملّی";
            Gridview_Member.Columns[7].HeaderText = "تلفن";
            Gridview_Member.Columns[8].HeaderText = "ایمیل";
            Gridview_Member.Columns[9].HeaderText = "آدرس";
            Gridview_Member.Columns[10].HeaderText = "تاریخ عضویت";
            Gridview_Member.Columns[11].HeaderText = "تاریخ اتمام";
            Gridview_Member.Columns[12].HeaderText = "مدت عضویت";
            Gridview_Member.Columns[13].HeaderText = "مبلغ عضویت";

            Gridview_Member.Columns[1].Width = 80;
            Gridview_Member.Columns[2].Width = 90;
            Gridview_Member.Columns[3].Width = 105;
            Gridview_Member.Columns[4].Width = 80;
            Gridview_Member.Columns[5].Width = 100;
            Gridview_Member.Columns[6].Width = 85;
            Gridview_Member.Columns[7].Width = 85;
            Gridview_Member.Columns[10].Width = 75;
            Gridview_Member.Columns[11].Width = 75;
            Gridview_Member.Columns[12].Width = 50;
            Gridview_Member.Columns[13].Width = 80;

            Gridview_Member.Columns[0].Visible = false;
            Gridview_Member.Columns[8].Visible = false;
            Gridview_Member.Columns[9].Visible = false;
        }
        //...تابع ویرایش بخش هدر گریدویو

        public void EBtn()
        {
            btn_new.Enabled = true;
            btn_save.Enabled = false;
            btn_edit.Enabled = true;
            btn_delete.Enabled = true;
        }

        public void DBtn()
        {
            btn_new.Enabled = false;
            btn_save.Enabled = true;
            btn_edit.Enabled = false;
            btn_delete.Enabled = false;
        }

        public void FullGrid_Member()
        {
            dbs.connect();
            Gridview_Member.DataSource = dbs.show("Select * From MemberInfo");
            dbs.disconnect();
            HeaderEdit();
        }

        public void Clear()
        {
            txt_address.Text = null;
            txt_modat.Text = "0";
            txt_code_meli.Text = null;
            txt_edate.Text = null;
            txt_email.Text = null;
            txt_fname.Text = null;
            txt_lname.Text = null;
            txt_pname.Text = null;
            txt_sdate.Text = null;
            txt_shenas.Text = null;
            txt_tel.Text = null;
            com_madrak.Text = null;
            txt_price.Text = "0";
        }

        public void IndexGridView()
        {
            id = Gridview_Member[0, Gridview_Member.CurrentRow.Index].Value.ToString();
            txt_shenas.Text = Gridview_Member[1, Gridview_Member.CurrentRow.Index].Value.ToString();
            txt_fname.Text = Gridview_Member[2, Gridview_Member.CurrentRow.Index].Value.ToString();
            txt_lname.Text = Gridview_Member[3, Gridview_Member.CurrentRow.Index].Value.ToString();
            txt_pname.Text = Gridview_Member[4, Gridview_Member.CurrentRow.Index].Value.ToString();
            com_madrak.Text = Gridview_Member[5, Gridview_Member.CurrentRow.Index].Value.ToString();
            txt_code_meli.Text = Gridview_Member[6, Gridview_Member.CurrentRow.Index].Value.ToString();
            txt_tel.Text = Gridview_Member[7, Gridview_Member.CurrentRow.Index].Value.ToString();
            txt_email.Text = Gridview_Member[8, Gridview_Member.CurrentRow.Index].Value.ToString();
            txt_address.Text = Gridview_Member[9, Gridview_Member.CurrentRow.Index].Value.ToString();
            txt_sdate.Text = Gridview_Member[10, Gridview_Member.CurrentRow.Index].Value.ToString();
            txt_edate.Text = Gridview_Member[11, Gridview_Member.CurrentRow.Index].Value.ToString();
            txt_modat.Text = Gridview_Member[12, Gridview_Member.CurrentRow.Index].Value.ToString();
            txt_price.Text = Gridview_Member[13, Gridview_Member.CurrentRow.Index].Value.ToString();
        }

        private void btn_new_Click(object sender, EventArgs e)
        {
            txt_shenas.Focus();
            Clear();
            dbs.disconnect();
            FullGrid_Member();
            DBtn();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            dbs.connect();
            dt = dbs.show("Select * From MemberInfo Where shenas_member='" + txt_shenas.Text + "'");
            if (dt.Rows.Count > 0)
            {
                MessageBox.Show("کاربر محترم شناسه عضو تکراری می باشد", "اخطار", MessageBoxButtons.OK, MessageBoxIcon.Error);
                dbs.disconnect();
            }
            else
            {
                dbs.execute("Insert Into MemberInfo Values('" + txt_shenas.Text + "',N'" + txt_fname.Text + "',N'" + txt_lname.Text + "',N'" + txt_pname.Text + "',N'" + com_madrak.Text + "','" + txt_code_meli.Text + "','" + txt_tel.Text + "','" + txt_email.Text + "',N'" + txt_address.Text + "','" + txt_sdate.Text + "','" + txt_edate.Text + "','" + txt_modat.Text + "','" + txt_price.Text + "')");
                dbs.disconnect();
                FullGrid_Member();
                EBtn();
                Clear();
                CountRows();
                MessageBox.Show("کاربر محترم عملیات ثبت عضو با موفقیت انجام شد", "پیغام", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btn_edit_Click(object sender, EventArgs e)
        {
            dbs.connect();
            dbs.execute("Update MemberInfo Set shenas_member='" + txt_shenas.Text + "',fname=N'" + txt_fname.Text + "',lname=N'" + txt_lname.Text + "',pname=N'" + txt_pname.Text + "',madrak=N'" + com_madrak.Text + "',code_meli='" + txt_code_meli.Text + "',tel='" + txt_tel.Text + "',email='" + txt_email.Text + "',address=N'" + txt_address.Text + "',sdate='" + txt_sdate.Text + "',edate='" + txt_edate.Text + "',modat='" + txt_modat.Text + "',cost='" + txt_price.Text + "' Where ID='" + id + "'");
            dbs.disconnect();
            FullGrid_Member();
            EBtn();
            Clear();
            MessageBox.Show("کاربر محترم عملیات ثبت عضو با موفقیت انجام شد", "پیغام", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("کاربر محترم آیا مطمئن به حذف عضو مورد نظر می باشید؟", "هشدار", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                dbs.connect();
                dbs.execute("Delete From MemberInfo Where ID='" + id + "'");
                dbs.disconnect();
                FullGrid_Member();
                EBtn();
                CountRows();
                Clear();
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Clear();
            dbs.disconnect();
            FullGrid_Member();
            EBtn();
        }

        private void btn_list_book_Click(object sender, EventArgs e)
        {
            ListBorrowBook_Frm lbbf = new ListBorrowBook_Frm();
            lbbf.sh_member = txt_shenas.Text;
            lbbf.ShowDialog();
        }

        private void Member_Frm_Load(object sender, EventArgs e)
        {
            FullGrid_Member();
            CountRows();
        }

        private void Gridview_Member_Click(object sender, EventArgs e)
        {
            IndexGridView();
        }

        private void Gridview_Member_KeyDown(object sender, KeyEventArgs e)
        {
            IndexGridView();
        }

        private void Gridview_Member_KeyUp(object sender, KeyEventArgs e)
        {
            IndexGridView();
        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            dbs.connect();
            Gridview_Member.DataSource = dbs.show("Select * From MemberInfo Where shenas_member Like '%" + txt_search.Text + "%' or lname Like N'%" + txt_search.Text + "%' or code_meli Like '%" + txt_search.Text + "%' or sdate Like N'%" + txt_search.Text + "%'");
            dbs.disconnect();
            HeaderEdit();
            CountRows();
        }

        private void Gridview_Member_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 13 && e.RowIndex != this.Gridview_Member.NewRowIndex)
                {
                    double d = double.Parse(e.Value.ToString());
                    e.Value = d.ToString("#,##0.##");
                }

            }
            catch (Exception ex)
            {
            }
        }

        private void Gridview_Member_DoubleClick(object sender, EventArgs e)
        {
            btn_list_book_Click(sender, e);
        }

    }
}
