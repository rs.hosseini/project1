﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LibrarySoft
{
    public partial class SearchMember_Frm : Form
    {
        public string shenas, name;
        public SearchMember_Frm()
        {
            InitializeComponent();
        }

        Class_DBS dbs = new Class_DBS();

        public void HeaderEdit()
        {
            Gridview_Member.Columns[1].HeaderText = "شماره عضویت";
            Gridview_Member.Columns[2].HeaderText = "نام";
            Gridview_Member.Columns[3].HeaderText = "نام خانوادگی";
            Gridview_Member.Columns[10].HeaderText = "تاریخ عضویت";

            Gridview_Member.Columns[1].Width = 90;
            Gridview_Member.Columns[2].Width = 105;
            Gridview_Member.Columns[3].Width = 130;
            Gridview_Member.Columns[10].Width = 100;

            Gridview_Member.Columns[0].Visible = false;
            Gridview_Member.Columns[4].Visible = false;
            Gridview_Member.Columns[5].Visible = false;
            Gridview_Member.Columns[6].Visible = false;
            Gridview_Member.Columns[7].Visible = false;
            Gridview_Member.Columns[8].Visible = false;
            Gridview_Member.Columns[9].Visible = false;
            Gridview_Member.Columns[11].Visible = false;
            Gridview_Member.Columns[12].Visible = false;
            Gridview_Member.Columns[13].Visible = false;
        }
        //...تابع ویرایش بخش هدر گریدویو

        public void FullGrid_Member()
        {
            dbs.connect();
            Gridview_Member.DataSource = dbs.show("Select * From MemberInfo");
            dbs.disconnect();
            HeaderEdit();
        }

        public void RowIndex()
        {
            shenas = Gridview_Member[1, Gridview_Member.CurrentRow.Index].Value.ToString();
            name = Gridview_Member[2, Gridview_Member.CurrentRow.Index].Value.ToString() + " " + Gridview_Member[3, Gridview_Member.CurrentRow.Index].Value.ToString();
        }

        private void SearchMember_Frm_Load(object sender, EventArgs e)
        {
            FullGrid_Member();
        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            dbs.connect();
            Gridview_Member.DataSource = dbs.show("Select * From MemberInfo Where shenas_member Like '%" + txt_search.Text + "%' or lname Like N'%" + txt_search.Text + "%' or code_meli Like '%" + txt_search.Text + "%' or sdate Like N'%" + txt_search.Text + "%'");
            dbs.disconnect();
            HeaderEdit();
        }

        private void Gridview_Member_DoubleClick(object sender, EventArgs e)
        {
            RowIndex();
            this.Close();
        }
    }
}
