﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Data.SqlClient;

public class Class_DBS
{
    private SqlConnection cn;
    private SqlCommand cm;
    private SqlDataAdapter da;

    public Class_DBS()
    {
        cn = new SqlConnection();
        cm = new SqlCommand();
        da = new SqlDataAdapter();
        cm.Connection = cn;
        da.SelectCommand = cm;

        
    }

    public void connect()
    {
      string cs = "Data Source=.;Initial Catalog=Library;Integrated Security=True";
      cn.ConnectionString = cs;
      cn.Open();
    }

    public void disconnect()
    {
        cn.Close();
    }

    public DataTable show(string query)
    {
        DataTable dt = new DataTable();
        cm.CommandText = query;
        da.Fill(dt);
        return dt;
    }

    public void execute(string query)
    {
        cm.CommandText = query;
        cm.ExecuteNonQuery();
    }
}